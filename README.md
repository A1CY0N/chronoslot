# chronoSlot
> Automatic detection of free slots Chronodrive

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg?style=flat-square)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
[![Build Status][travis-image]][travis-url]


Chronoslot is a shell script to detect the free slots available at Chronodrive and notify the user with a telegram bot.

![](header.png)

## Installation

OS X & Linux:

1. Install chronoSlot tool
    ```sh
    git clone https://gitlab.com/A1CY0N/chronoslot.git
    cd chronoslot
    chmod +x chronoSlot.sh
    ```
2. Install Telegram-notify tool
    ```sh
    wget https://raw.githubusercontent.com/NicolasBernaerts/debian-scripts/master/telegram/telegram-notify-install.sh
    chmod +x telegram-notify-install.sh
    ./telegram-notify-install.sh

    ```
3. Create a telegram bot : Follow steps 1 to 13 here : https://github.com/topkecleon/telegram-bot-bash 
    /!\ Note down the API key given by @BotFather

4. Get user ID :
    + Send a message to your Bot from your Telegram client
    + Call the following URL from any web browser. XXXXX = your API key.
        https://api.telegram.org/botXXXXX/getUpdates
        In the page displayed, you'll get some information. search for "from":"id":YYYYY, ". YYYYY is your user ID.

5. Update /etc/telegram-notify.conf with your telegram API key and user ID.

## Usage example

Before using chronoSlot, you need to find the id of your favorite store. To do so, you have to go to the following url: [https://www.chronodrive.com/magasins-chronodrive](https://www.chronodrive.com/magasins-chronodrive)

By clicking on the link of your store, you arrive on a URL like : 
[https://www.chronodrive.com/magasins-chronodrive/drive-magasin-1023-pessac](https://www.chronodrive.com/magasins-chronodrive/drive-magasin-1023-pessac). 

The unique store identifier is the 4 digits between "https://www.chronodrive.com/magasins-chronodrive/drive-magasin-" and the store name.

We can now use the awesome script :
```sh
./chronoSlot.sh 1234
```
1234 is the shop id of your chronodrive.

_**So by running the script you will be notified on telegram of the next timetable available at chronodrive.**_


## Release History

* 0.3.0
    * ADD: LICENSE
* 0.2.1
    * CHANGE: README.md
* 0.2.0
    * ADD: telegram notifications 
* 0.1.0
    * The first proper release
    * CHANGE: README.md
    * ADD: chronoSlot.sh
* 0.0.1
    * ADD: Add README.md

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/chronoslot>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
