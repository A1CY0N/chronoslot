#!/bin/bash

# Check arguments
if [ $# -ne 1 ] || ! [[ "$1" =~ ^[0-9]+$ ]]
then
        echo "Please provide the shop id as an argument."
        exit 1
fi

# Chrono Shop ID
store_id=$1

# Get index content with Shop ID
page="$(curl -b 'chronoShop="shopId='$store_id'"' https://www.chronodrive.com/home)"

# Extract Store Name & Next Free Slot
next_slot="$(echo $page | grep -P -o '<div class="dispo dispo--empty">.*?</div>' | sed -e 's/<[^>]*>//g')"
store_name="$(echo $page | grep -P -o '<div class="nMag">.*?</div>' | sed -e 's/<[^>]*>//g')"

# If I have an available slot I send msg via telegram
if ! [ -z "$store_name" ]
then
        if [ ${#next_slot} -ne 0 ]
        then
                echo "$next_slot" | telegram-notify --success --text "Chronodrive $store_name"
                telegram-notify --success --text "Chronodrive $store_name has an available slot : $next_slot !"
        else
                echo "Chronodrive $store_name has no available slot..."
        fi
fi
